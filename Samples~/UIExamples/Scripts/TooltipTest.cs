using UnityEngine;
using UnityEngine.InputSystem;

namespace Walid.UI.Test
{
    public class TooltipTest : MonoBehaviour
    {
        public void OnFire(InputAction.CallbackContext context)
        {
            TooltipManager.Singleton.ShowToolTip("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.");
        }
    } 
}