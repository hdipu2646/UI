using System;
using UnityEngine;
using UnityEngine.UI;

namespace Walid.UI
{
    [Serializable]
    public struct ProgressBar
    {
        public GameObject gameObject;
        public Text statusText, valueText;
        public Image fillImage;
        public float Value
        {
            set
            {
                fillImage.fillAmount = value;
                valueText.text = string.Format("{0}%", Math.Round(value * 100), 0);
            }
        }

        public void Reset()
        {
            statusText.text = valueText.text = string.Empty;
            gameObject.SetActive(false);
            Value = 0;
        }
    }
}
